FROM golang:1.11-alpine3.8 AS build
WORKDIR /go/src/gitlab.com/evgeny08/service-structure
COPY . /go/src/gitlab.com/evgeny08/service-structure
RUN apk add git
RUN go get -u github.com/golang/dep/cmd/dep
RUN dep ensure
RUN CGO_ENABLED=0 go build -o /out/service-structure gitlab.com/evgeny08/service-structure/cmd/service-structure-d

# copy to alpine image
FROM alpine:3.8
WORKDIR /app
COPY --from=build /out/service-structure /app
CMD ["/app/service-structure"]

package types

type Structure struct {
	ID   string `json:"id"   bson:"_id"`
	Name string `json:"name" bson:"name"`
	Type string `json:"type" bson:"type"`
	Path string `json:"path" bson:"path"`
}

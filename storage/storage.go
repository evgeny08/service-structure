package storage

import (
	"errors"
	"sync"

	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/log/level"
	"gopkg.in/mgo.v2"
)

var db *mgo.Database

const (
	collectionStructure = "structure_main"
)

// Storage stores terminals.
type Storage struct {
	url    string
	dbName string
	logger log.Logger

	mu      sync.RWMutex
	session *mgo.Session // Master session.
	lastErr error
}

// Config is a storage configuration.
type Config struct {
	URL    string
	Logger log.Logger
	DBName string
}

// New creates a new MongoDB storage using the given configuration.
func New(cfg *Config) (*Storage, error) {
	s := &Storage{
		url:    cfg.URL,
		dbName: cfg.DBName,
		logger: cfg.Logger,
	}

	err := s.connect(cfg)
	if err != nil {
		return nil, err
	}
	return s, nil
}

func (s *Storage) connect(cfg *Config) error {
	for {
		session, err := mgo.Dial(cfg.URL)
		if err != nil {
			return err
		}

		db = session.DB(cfg.DBName)
		level.Info(s.logger).Log("msg", "established mongo connection")
		s.mu.Lock()
		s.session = session.Copy()
		s.mu.Unlock()
		return nil
	}
}

// Shutdown close mongo session
func (s *Storage) Shutdown() {
	// Close mongo session.
	s.mu.Lock()
	if s.session != nil {
		s.session.Close()
		s.session = nil
		s.lastErr = errors.New("mongoclient is shut down")
	}
	s.mu.Unlock()

	level.Info(s.logger).Log("msg", "mongoclient: shutdown complete")
}

package storage

import (
	"context"
	"gopkg.in/mgo.v2/bson"

	"gitlab.com/evgeny08/service-structure/types"
)

// CreateStructure create new structure
func (s *Storage) CreateStructure(ctx context.Context, structure *types.Structure) error {
	err := db.C(collectionStructure).Insert(&structure)
	return err
}

// GetStructure get structure by id
func (s *Storage) GetStructure(ctx context.Context, id string) (*types.Structure, error) {
	var structure *types.Structure
	err := db.C(collectionStructure).Find(bson.M{"_id": id}).One(&structure)
	return structure, err
}

// GetListStructure return the list structures
func (s *Storage) GetListStructure(ctx context.Context) ([]*types.Structure, error) {
	var structure []*types.Structure
	err := db.C(collectionStructure).Find(bson.M{}).All(&structure)
	return structure, err
}

package httpserver

import (
	"context"
	"net/url"

	"github.com/go-kit/kit/endpoint"
	kithttp "github.com/go-kit/kit/transport/http"

	"gitlab.com/evgeny08/service-structure/types"
)

// Client is a client for api service.
type Client struct {
	createStructure  endpoint.Endpoint
	getStructure     endpoint.Endpoint
	getListStructure endpoint.Endpoint
}

// NewClient creates a new service client.
func NewClient(serviceURL string) (*Client, error) {
	baseURL, err := url.Parse(serviceURL)
	if err != nil {
		return nil, err
	}

	c := &Client{
		createStructure: kithttp.NewClient(
			"POST",
			baseURL,
			encodeCreateStructureRequest,
			decodeCreateStructureResponse,
		).Endpoint(),

		getStructure: kithttp.NewClient(
			"GET",
			baseURL,
			encodeGetStructureRequest,
			decodeGetStructureResponse,
		).Endpoint(),

		getListStructure: kithttp.NewClient(
			"GET",
			baseURL,
			encodeGetListStructureRequest,
			decodeGetListStructureResponse,
		).Endpoint(),
	}

	return c, nil
}

func (c *Client) CreateStructure(ctx context.Context, name string) (*types.Structure, error) {
	request := createStructureRequest{Name: name}
	response, err := c.createStructure(ctx, request)
	if err != nil {
		return nil, err
	}
	res := response.(createStructureResponse)
	return res.Structure, res.Err
}

func (c *Client) GetStructure(ctx context.Context, id string) (*types.Structure, error) {
	request := getStructureRequest{ID: id}
	response, err := c.getStructure(ctx, request)
	if err != nil {
		return nil, err
	}
	res := response.(getStructureResponse)
	return res.Structure, res.Err
}

func (c *Client) GetListStructure(ctx context.Context) ([]*types.Structure, error) {
	var request interface{}
	response, err := c.getListStructure(ctx, request)
	if err != nil {
		return nil, err
	}
	res := response.(getListStructureResponse)
	return res.ListStructure, res.Err
}

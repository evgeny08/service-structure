package httpserver

import (
"context"
"github.com/go-kit/kit/log"
"golang.org/x/time/rate"
"net/http/httptest"
"reflect"
"testing"

"gitlab.com/evgeny08/service-structure/types"
)

type mockService struct {
	onCreateStructure func(ctx context.Context, name string) (*types.Structure, error)
	onGetStructure func(ctx context.Context, id string) (*types.Structure, error)
	onGetListStructure func(ctx context.Context) ([]*types.Structure, error)
}

func (s *mockService) createStructure(ctx context.Context, name string) (*types.Structure, error) {
	return s.onCreateStructure(ctx, name)
}

func (s *mockService) getStructure(ctx context.Context, id string) (*types.Structure, error) {
	return s.onGetStructure(ctx, id)
}

func (s *mockService) getListStructure(ctx context.Context) ([]*types.Structure, error) {
	return s.onGetListStructure(ctx)
}

func startTestServer(t *testing.T) (*httptest.Server, *Client, *mockService) {
	svc := &mockService{}

	handler := newHandler(&handlerConfig{
		svc:         svc,
		logger:      log.NewNopLogger(),
		rateLimiter: rate.NewLimiter(rate.Inf, 1),
	})

	server := httptest.NewServer(handler)

	client, err := NewClient(server.URL)
	if err != nil {
		t.Fatal(err)
	}

	return server, client, svc
}

func TestCreateStructure(t *testing.T) {
	server, client, svc := startTestServer(t)
	defer server.Close()

	testCases := []struct {
		name string
		structureName string
		structure *types.Structure
		err  error
	}{
		{
			name: "ok response",
			structureName: "21",
			structure: &types.Structure{
				ID:   "1",
				Name: "21",
				Type: "organization",
				Path: ",2,",
			},
			err: nil,
		},
		{
			name: "err response",
			structureName: "",
			err: errorf(ErrNotFound, "404 page not found"),
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			svc.onCreateStructure = func(ctx context.Context, structureName string) (*types.Structure, error) {
				return tc.structure, tc.err
			}
			gotStructure, gotErr := client.CreateStructure(context.Background(), tc.structureName)
			if !reflect.DeepEqual(gotStructure, tc.structure) {
				t.Fatalf("got structure %#v want %#v", gotStructure, tc.structure)
			}
			if !reflect.DeepEqual(gotErr, tc.err) {
				t.Fatalf("got error %#v want %#v", gotErr, tc.err)
			}


		})
	}
}

func TestGetStructure(t *testing.T) {
	server, client, svc := startTestServer(t)
	defer server.Close()

	testCases := []struct {
		name string
		id string
		structure *types.Structure
		err  error
	}{
		{
			name: "ok response",
			id: "00",
			structure: &types.Structure{
				ID:   "45",
				Name: "21",
				Type: "organization",
				Path: ",2,",
			},
			err: nil,
		},
		{
			name: "err response",
			id: "",
			err: errorf(ErrNotFound, "404 page not found"),
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			svc.onGetStructure = func(ctx context.Context, id string) (*types.Structure, error) {
				return tc.structure, tc.err
			}
			gotStructure, gotErr := client.GetStructure(context.Background(), tc.id)
			if !reflect.DeepEqual(gotStructure, tc.structure) {
				t.Fatalf("got structure %#v want %#v", gotStructure, tc.structure)
			}
			if !reflect.DeepEqual(gotErr, tc.err) {
				t.Fatalf("got error %#v want %#v", gotErr, tc.err)
			}


		})
	}
}

func TestGetListStructure(t *testing.T) {
	server, client, svc := startTestServer(t)
	defer server.Close()

	testCases := []struct {
		name string
		structure []*types.Structure
		err  error
	}{
		{
			name: "ok response",
			structure: []*types.Structure{
				{
					ID:   "11",
					Name: "dfdf",
					Type: "33",
					Path: "5",
				},
				{
					ID:   "464",
					Name: "fhjf",
					Type: "555",
					Path: "334",
				},
			},
			err: nil,
		},
		{
			name: "err response",
			err: errorf(ErrNotFound, "404 page not found"),
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			svc.onGetListStructure = func(ctx context.Context) ([]*types.Structure, error) {
				return tc.structure, tc.err
			}
			gotStructure, gotErr := client.GetListStructure(context.Background())
			if !reflect.DeepEqual(gotStructure, tc.structure) {
				t.Fatalf("got structure %#v want %#v", gotStructure, tc.structure)
			}
			if !reflect.DeepEqual(gotErr, tc.err) {
				t.Fatalf("got error %#v want %#v", gotErr, tc.err)
			}


		})
	}
}

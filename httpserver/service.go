package httpserver

import (
	"context"
	"errors"
	"strings"

	"github.com/go-kit/kit/log"
	"github.com/google/uuid"

	"gitlab.com/evgeny08/service-structure/types"
)

// service manages HTTP server methods.
type service interface {
	createStructure(ctx context.Context, name string) (*types.Structure, error)
	getStructure(ctx context.Context, id string) (*types.Structure, error)
	getListStructure(ctx context.Context) ([]*types.Structure, error)
}

type basicService struct {
	logger  log.Logger
	storage Storage
}

func (s *basicService) createStructure(ctx context.Context, name string) (*types.Structure, error) {
	// validation
	if strings.TrimSpace(name) == "" {
		return nil, errors.New("empty name")
	}

	// gen structure
	id := uuid.New()
	structure := &types.Structure{
		ID:   id.String(),
		Name: name,
		Type: "",
		Path: "",
	}

	// create structure in storage
	err := s.storage.CreateStructure(ctx, structure)
	if err != nil {
		return nil, err
	}
	return structure, nil
}

// get structure by  id
func (s *basicService) getStructure(ctx context.Context, id string) (*types.Structure, error) {
	if strings.TrimSpace(id) == "" {
		return  nil, errorf(ErrBadParams, "empty id")
	}
	structure, err := s.storage.GetStructure(ctx, id)
	if err != nil {
		return nil, err
	}
	return structure, nil
}

// GetListStructure return the list structure
func (s *basicService) getListStructure(ctx context.Context) ([]*types.Structure, error) {
	listStructure, err := s.storage.GetListStructure(ctx)
	if err != nil {
		if storageErrIsNotFound(err) {
			return nil, errorf(ErrNotFound, "Structures is not found")
		}
		return nil, errorf(ErrInternal, "failed to get structures: %v", err)
	}
	return listStructure, nil
}

// storageErrIsNotFound checks if the storage error is "not found".
func storageErrIsNotFound(err error) bool {
	type notFound interface {
		NotFound() bool
	}
	e, ok := err.(notFound)
	return ok && e.NotFound()
}



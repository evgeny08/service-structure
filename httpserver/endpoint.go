package httpserver

import (
	"context"
	"github.com/go-kit/kit/endpoint"

	"gitlab.com/evgeny08/service-structure/types"
)

func makeCreateStructureEndpoint(svc service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(createStructureRequest)
		res, err := svc.createStructure(ctx, req.Name)
		return createStructureResponse{Structure: res, Err: err}, nil
	}
}

type createStructureRequest struct {
	Name string
}

type createStructureResponse struct {
	Structure *types.Structure
	Err error
}

func makeGetStructureEndpoint(svc service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(getStructureRequest)
		structure, err := svc.getStructure(ctx, req.ID)
		return getStructureResponse{Structure: structure, Err: err}, nil
	}
}

type getStructureRequest struct {
	ID string
}

type getStructureResponse struct {
	Structure *types.Structure
	Err error
}

func makeGetListStructureEndpoint(svc service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		listStructure, err := svc.getListStructure(ctx)
		return getListStructureResponse{ListStructure: listStructure, Err: err}, nil
	}
}

type getListStructureResponse struct {
	ListStructure []*types.Structure
	Err error
}


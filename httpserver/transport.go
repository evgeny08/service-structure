package httpserver

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"strings"

	"github.com/gorilla/mux"

	"gitlab.com/evgeny08/service-structure/types"
)

// Service.CreateStructure encoders/decoders.
func encodeCreateStructureRequest(_ context.Context, r *http.Request, request interface{}) error {
	req := request.(createStructureRequest)
	r.URL.Path = "/api/v1/structure/" + url.QueryEscape(req.Name)
	return nil
}

func decodeCreateStructureRequest(_ context.Context, r *http.Request) (interface{}, error) {
	var req createStructureRequest
	req.Name = mux.Vars(r)["name"]
	return req, nil
}

func encodeCreateStructureResponse(_ context.Context, w http.ResponseWriter, response interface{}) error {
	res := response.(createStructureResponse)
	if res.Err != nil {
		return encodeError(w, res.Err, true)
	}
	w.WriteHeader(http.StatusCreated)
	w.Header().Set("Content-Type", "application/json")
	return json.NewEncoder(w).Encode(res.Structure)
}

func decodeCreateStructureResponse(_ context.Context, r *http.Response) (interface{}, error) {
	if r.StatusCode < 200 || r.StatusCode > 299 {
		return createStructureResponse{Err: decodeError(r)}, nil
	}
	res := createStructureResponse{Structure: &types.Structure{}}
	err := json.NewDecoder(r.Body).Decode(&res.Structure)
	return res, err
}

// Service.GetStructure encoders/decoders.
func encodeGetStructureRequest(_ context.Context, r *http.Request, request interface{}) error {
	req := request.(getStructureRequest)
	r.URL.Path = "/api/v1/structure/" + url.QueryEscape(req.ID)
	return nil
}

func decodeGetStructureRequest(_ context.Context, r *http.Request) (interface{}, error) {
	id := mux.Vars(r)["id"]
	return getStructureRequest{ID : id}, nil
}

func encodeGetStructureResponse(_ context.Context, w http.ResponseWriter, response interface{}) error {
	res := response.(getStructureResponse)
	if res.Err != nil {
		return encodeError(w, res.Err, true)
	}
	w.Header().Set("Content-Type", "application/json")
	return json.NewEncoder(w).Encode(res.Structure)
}

func decodeGetStructureResponse(_ context.Context, r *http.Response) (interface{}, error) {
	if r.StatusCode < 200 || r.StatusCode > 299 {
		return getStructureResponse{Err: decodeError(r)}, nil
	}
	res := getStructureResponse{Structure: &types.Structure{}}
	err := json.NewDecoder(r.Body).Decode(&res.Structure)
	return res, err
}

// Service.GetListStructure encoders/decoders.
func encodeGetListStructureRequest(_ context.Context, r *http.Request, _ interface{}) error {
	r.URL.Path = "/api/v1/structure"
	return nil
}

func decodeGetListStructureRequest(_ context.Context, _ *http.Request) (interface{}, error) {
	return nil, nil
}

func encodeGetListStructureResponse(_ context.Context, w http.ResponseWriter, response interface{}) error {
	res := response.(getListStructureResponse)
	if res.Err != nil {
		return encodeError(w, res.Err, true)
	}
	w.Header().Set("Content-Type", "application/json")
	return json.NewEncoder(w).Encode(res.ListStructure)
}

func decodeGetListStructureResponse(_ context.Context, r *http.Response) (interface{}, error) {
	if r.StatusCode < 200 || r.StatusCode > 299 {
		return getListStructureResponse{Err: decodeError(r)}, nil
	}
	res := getListStructureResponse{ListStructure: []*types.Structure{}}
	err := json.NewDecoder(r.Body).Decode(&res.ListStructure)
	return res, err
}

// errKindToStatus maps service error kinds to the HTTP response codes.
var errKindToStatus = map[ErrorKind]int{
	ErrBadParams: http.StatusBadRequest,
	ErrNotFound:  http.StatusNotFound,
	ErrConflict:  http.StatusConflict,
	ErrInternal:  http.StatusInternalServerError,
}

// encodeError writes a service error to the given http.ResponseWriter.
func encodeError(w http.ResponseWriter, err error, writeMessage bool) error {
	status := http.StatusInternalServerError
	message := err.Error()
	if err, ok := err.(*Error); ok {
		if s, ok := errKindToStatus[err.Kind]; ok {
			status = s
		}
		if err.Kind == ErrInternal {
			message = "internal error"
		} else {
			message = err.Message
		}
	}
	w.WriteHeader(status)
	if writeMessage {
		_, writeErr := io.WriteString(w, message)
		return writeErr
	}
	return nil
}

// decodeError reads a service error from the given *http.Response.
func decodeError(r *http.Response) error {
	var buf bytes.Buffer
	if _, err := io.Copy(&buf, io.LimitReader(r.Body, 1024)); err != nil {
		return fmt.Errorf("%d: %s", r.StatusCode, http.StatusText(r.StatusCode))
	}
	msg := strings.TrimSpace(buf.String())
	if msg == "" {
		msg = http.StatusText(r.StatusCode)
	}
	for kind, status := range errKindToStatus {
		if status == r.StatusCode {
			return &Error{Kind: kind, Message: msg}
		}
	}
	return fmt.Errorf("%d: %s", r.StatusCode, msg)
}

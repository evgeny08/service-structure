package httpserver

import (
	"context"
	"time"

	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/log/level"

	"gitlab.com/evgeny08/service-structure/types"
)

// loggingMiddleware wraps Service and logs request information to the provided logger.
type loggingMiddleware struct {
	next   service
	logger log.Logger
}

func (m *loggingMiddleware) createStructure(ctx context.Context, name string) (*types.Structure, error) {
	begin := time.Now()
	res, err := m.next.createStructure(ctx, name)
	level.Info(m.logger).Log(
		"method", "createStructure",
		"err", err,
		"elapsed", time.Since(begin),
	)
	return res, err
}

func (m *loggingMiddleware) getStructure(ctx context.Context, id string) (*types.Structure, error) {
	begin := time.Now()
	structure, err := m.next.getStructure(ctx, id)
	level.Info(m.logger).Log(
		"method", "getStructure",
		"err", err,
		"elapsed", time.Since(begin),
		"id", structure.ID,
	)
	return structure, err
}

func (m *loggingMiddleware) getListStructure(ctx context.Context) ([]*types.Structure, error) {
	begin := time.Now()
	listStructure, err := m.next.getListStructure(ctx)
	level.Info(m.logger).Log(
		"method", "getListStructure",
		"err", err,
		"elapsed", time.Since(begin),
	)
	return listStructure, err
}
